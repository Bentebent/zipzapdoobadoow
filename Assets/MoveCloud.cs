﻿using UnityEngine;
using System.Collections;

public class MoveCloud : MonoBehaviour {


	// Use this for initialization
	void Start () {

        Vector3 position;
        int maxItterations = 1000;
        position = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0.0f).normalized;
        for (int i = 0; i < maxItterations; i++)
        {
            if (position.x > 0.0f || position.y < 0.0f)
                break;

            position = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0.0f).normalized;
        }


        position *= Random.Range( 40.0f, 410.0f );
        position.z = Random.Range( -300.0f, 200.0f );

        gameObject.transform.position = position;

	}
	
	// Update is called once per frame
	void Update () {

        
        gameObject.transform.Translate( new Vector3( 0.0f, 0.0f, 1.0f ) * Time.deltaTime * 2.0f );
        if( gameObject.transform.position.z > 200.0f )
        {
            Vector3 newPos = gameObject.transform.position;
            newPos.z = -200.0f;
            gameObject.transform.position = newPos;
        }


	}
}
