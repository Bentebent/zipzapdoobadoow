﻿using UnityEngine;
using System.Collections;

public class HandleCubes : MonoBehaviour {

    public int numberOfCubes;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
        GameObject[] cubes;
        cubes = GameObject.FindGameObjectsWithTag("PickableCube");
        if( cubes.Length < numberOfCubes )
        {
            Instantiate( Resources.Load("Cube") );
        }
        
	
	}
}
