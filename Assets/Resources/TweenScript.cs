﻿using UnityEngine;
using System.Collections;

public class TweenScript : MonoBehaviour {

	// Use this for initialization

    float maxScale = 0.7f;
    float minScale = 0.2f;
    bool downX;
    bool downZ;
    float tweenFactor = 80.0f;
	void Start () 
    {
        float xScale = Random.Range(minScale, maxScale);
        float zScale = Random.Range(minScale, maxScale);
        transform.localScale = new Vector3(xScale, transform.localScale.y, zScale);
	}
	
	// Update is called once per frame
	void Update () 
    {
        float xScale;
        float zScale;
        if (transform.localScale.x < minScale)
            downX = false;
        else if (transform.localScale.x > maxScale)
            downX = true;

        if (transform.localScale.z < minScale)
            downZ = false;
        else if (transform.localScale.z < maxScale)
            downZ = true;

        if (downX)
            xScale = WeightedAverage(transform.localScale.x, tweenFactor, minScale - 0.1f);
        else
            xScale = WeightedAverage(transform.localScale.x, tweenFactor, maxScale + 0.1f);


        if (downZ)
            zScale = WeightedAverage(transform.localScale.z, tweenFactor, minScale - 0.1f);
        else
            zScale = WeightedAverage(transform.localScale.z, tweenFactor, maxScale + 0.1f);

        transform.localScale = new Vector3(xScale, transform.localScale.y, zScale);
	}

    float WeightedAverage(float v, float N, float W)
    {
        return ((v * (N - 1)) + W) / N;
    }
}
