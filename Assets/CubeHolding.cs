﻿using UnityEngine;
using System.Collections;

public class CubeHolding : MonoBehaviour {

    public GameObject m_cube;
    public float pickupDistance = 2.5f;
    private AudioSource[] m_audioSources;
	// Use this for initialization
	void Start () {
        m_cube = null;
        m_audioSources = GetComponents<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        // if not holdning a cube, check for close cubes...
        if (m_cube == null)
        {
            GameObject[] cubes;
            cubes = GameObject.FindGameObjectsWithTag("PickableCube");
            Vector3 myPos = gameObject.transform.position;
            foreach (GameObject cubeObject in cubes)
            {
                Vector3 position = cubeObject.transform.position;
                float distance = (position - myPos).magnitude;
                CubePhysics cubePhysic = cubeObject.GetComponent<CubePhysics>();
                if (cubePhysic != null)
                {
                    if (distance < pickupDistance && cubePhysic.m_state == CubePhysics.CubeState.idle)
                    {
                        m_cube = cubeObject;
                        cubePhysic.m_state = CubePhysics.CubeState.held;
                        
                        PlayerController pcont = GetComponent<PlayerController>();
                        if( pcont )
                        {
                            cubePhysic.m_ownerName = pcont.m_playerName;
                            m_audioSources[2].Play();
                        }
                    }
                }
            }
        }

        // lose the cube...
        if (m_cube != null)
        {
            CubePhysics cubePhysic = m_cube.GetComponent<CubePhysics>();
            if (cubePhysic != null)
            {
                if (cubePhysic.m_state != CubePhysics.CubeState.held)
                {
                    m_cube = null;
                }
            }
        }

        // parenting...
        if (m_cube != null)
        {
            // copy position
            m_cube.transform.position = gameObject.transform.position;
            // offset height
            m_cube.transform.position += new Vector3(0.0f, 2.6f, 0.0f);
            // offset forward
            m_cube.transform.position += gameObject.transform.forward * 0.7f;
            // copy direction
            m_cube.transform.rotation = gameObject.transform.rotation;
        }

        // trowing stuff...
        if (m_cube != null)
        {
            PlayerController player = GetComponent<PlayerController>();
            if (player != null)
            {
                if (Input.GetAxis("RTrigger_Player" + player.m_playerName) < 0.0f)
                {
                    float height = -0.08f;
                    float strength = 100.0f;
                    Vector3 direction = (gameObject.transform.forward + gameObject.transform.up * height).normalized;
                    CubePhysics cubePhysic = m_cube.GetComponent<CubePhysics>();
                    if (cubePhysic != null)
                    {
                        cubePhysic.m_speed = direction * strength;
                        Debug.Log(direction * strength);
                        cubePhysic.m_state = CubePhysics.CubeState.thown;
                        m_audioSources[3].Play();
                    }

                    m_cube = null;
                    

                }
            }

        }




	}
}
