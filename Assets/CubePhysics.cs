﻿using UnityEngine;
using System.Collections;

public class CubePhysics : MonoBehaviour {

    public Vector3 m_speed;
    public float m_gravity;
    public string m_ownerName;

    private float m_bobOffset;

    public enum CubeState
    {
        idle, 
        held,
        thown,
        spawning,
        falling,
    };
    public CubeState m_state; 
    
	// Use this for initialization
	void Start () {
        Respawn();
        //gameObject.transform.position += new Vector3(0.0f, 1.0f, 0.0f) * Random.Range(-0.5f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () 
    {
        int levelMask = (1 << 9);
        Ray ray = new Ray(gameObject.transform.position, -gameObject.transform.up);
        RaycastHit hit;
        
        Physics.Raycast(gameObject.transform.position, -Vector3.up, out hit, levelMask);

        if (m_state == CubeState.thown )
        {
            if ( hit.distance < 1.0f && hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground") )
            {
                gameObject.transform.position += new Vector3(0.0f, 1.0f, 0.0f) * Time.deltaTime * 10.0f;
                m_speed = Vector3.Reflect(m_speed, new Vector3(0.0f, 1.0f, 0.0f)) * 0.5f;
                m_speed.y *= 0.5f;
            }
            else
            {
                gameObject.transform.position += m_speed * Time.deltaTime;
            }
        }
        else if (m_state == CubeState.spawning)
        {
            if (hit.distance < 1.5f && hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                gameObject.transform.position += new Vector3(0.0f, 1.0f, 0.0f) * Time.deltaTime * 10.0f;
                m_speed.y = 0.0f;
            }
            else
            {
                gameObject.transform.position += m_speed * Time.deltaTime;
            }
        }
        else if (m_state == CubeState.falling)
        {
            if (hit.distance < 1.0f && hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
            {
                gameObject.transform.position += new Vector3(0.0f, 1.0f, 0.0f) * Time.deltaTime * 10.0f;
                m_speed *= 0.6f;
                m_speed.y *= 0.05f;
            }
            else
            {
                gameObject.transform.position += m_speed * Time.deltaTime;
            }
        }
        

        // make it slow down...
        if (m_speed.magnitude < 0.4f && (m_state == CubeState.falling || m_state == CubeState.thown || m_state == CubeState.spawning))
        {
            m_speed = Vector3.zero;
            m_state = CubeState.idle;
        }

        // bobing...
        if (m_state == CubeState.idle)
        {
            float bobHeight = 0.05f;
            float bobSpeed = 5.0f;
            gameObject.transform.position += new Vector3(0.0f, 1.0f, 0.0f) * bobHeight * Mathf.Cos(Time.realtimeSinceStartup * bobSpeed + m_bobOffset);
        }

        if (gameObject.transform.position.y < -20.0f)
        {
            Respawn();
            Start();
        }

        m_speed += new Vector3(0.0f, -1.0f, 0.0f) * m_gravity * Time.deltaTime;
	}




    public void Respawn()
    {
        float spawHeight = 50.0f;
        gameObject.transform.position = new Vector3(Random.Range(-10.0f, 27.0f), Random.Range(20.0f, 40.0f) + spawHeight, Random.Range(-122.0f, -20.0f));
        m_state = CubeState.spawning;
        m_speed = new Vector3(0.0f, -20.0f, 0.0f);
        m_bobOffset = Random.Range(0.0f, 10.0f);
        m_ownerName = "NoOwner";
    }
}
