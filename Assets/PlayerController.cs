﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{

    public float m_maxSpeed;
    public float m_acceleration;
    public float m_currentSpeed;
    public float m_jumpSpeed;
    public float m_jumpStartSpeed;
    public float m_jumpAcceleration;
    public bool m_grounded = true;
    public bool m_jumping = false;

    public float m_oldX;
    public float m_oldZ;
    public bool m_blowHole;
    public float m_rotationSpeed;

    public bool m_allowControl = true;

    public int m_lives = 0;
    public int m_score = 0;

    public string m_playerName;

    private float threshold = 0.3f;
    public Vector3 forces;

    private CharacterController m_controller;
    private Rigidbody m_rigidbody;
    private CubeHolding m_cubeHolding;
    private AudioSource[] m_audioSources;

	// Use this for initialization
	void Start () 
    {
        m_controller = GetComponent<CharacterController>();
        m_rigidbody = GetComponent<Rigidbody>();
        m_cubeHolding = GetComponent<CubeHolding>();
        m_audioSources = GetComponents<AudioSource>();

        Material newMat = Resources.Load("Dust_" + m_playerName, typeof(Material)) as Material;
        gameObject.renderer.material = newMat;
        forces = new Vector3(0.0f, 0.0f, 0.0f);
	}
	
	// Update is called once per frame
	void Update () 
    {
        //UR DED LOL
        if (m_lives <= 0)
            return;

        float x = 0;
        float z = 0;

        if (m_allowControl)
        {
            x = Input.GetAxis("MoveHorizontal_Player" + m_playerName);
            z = Input.GetAxis("MoveVertical_Player" + m_playerName);
        }

        
        if ((Mathf.Abs(x) > threshold || Mathf.Abs(z) > threshold) && m_grounded)
        {
            if (m_currentSpeed < m_maxSpeed)
                m_currentSpeed += m_acceleration * Time.deltaTime;

            gameObject.transform.position += new Vector3(x, 0.0f, z).normalized * m_currentSpeed * Time.deltaTime;
            m_oldX = x;
            m_oldZ = z;

        }
        else if (!m_grounded)
        {
            if(Mathf.Abs(x) > threshold || Mathf.Abs(z) > threshold)
            {
                gameObject.transform.position += new Vector3(x, 0.0f, z).normalized * m_currentSpeed * 0.9f * Time.deltaTime;
                m_oldX = x;
                m_oldZ = z;
            }
            else
            {
                gameObject.transform.position += new Vector3(m_oldX, 0.0f, m_oldZ).normalized * m_currentSpeed * Time.deltaTime;
            }
        }
        else if (m_currentSpeed > 0.0f)
        {
            gameObject.transform.position += new Vector3(m_oldX, 0.0f, m_oldZ).normalized * m_currentSpeed * Time.deltaTime;
            m_currentSpeed -= m_acceleration * 1.5f * Time.deltaTime;
        }
        else if (m_currentSpeed <= 0.0f)
        {
            m_currentSpeed = 0.0f;
            m_oldX = 0.0f;
            m_oldZ = 0.0f;
        }

        if (m_currentSpeed > 0.0f)
        {
            if (!m_grounded)
            {
                GameObject go = Instantiate(Resources.Load("Dust_air") as GameObject) as GameObject;
                Material newMat = Resources.Load("Dust_" + m_playerName, typeof(Material)) as Material;
                go.renderer.material = newMat;
                go.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.5f, gameObject.transform.position.z);
            }
            else
            {
                GameObject go = Instantiate(Resources.Load("Dust") as GameObject) as GameObject;
                Material newMat = Resources.Load("Dust_" + m_playerName, typeof(Material)) as Material;
                go.renderer.material = newMat;
                go.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.5f, gameObject.transform.position.z);
            }
        }

        UpdateAnimations();


        Fall();
        HandleForces();

        if (m_allowControl)
        {
            Rotation();
            Jump();
        }        
	}

    private void HandleForces()
    {
        if( m_grounded )
        {
            forces *= 0.7f;
        }

        if( forces.magnitude < 0.5f )
        {
            forces = new Vector3(0.0f, 0.0f, 0.0f);
        }
        else
        {
            forces *= 1.0f - Time.deltaTime;
            gameObject.transform.position += forces * Time.deltaTime;
        }
    }

    private void Rotation()
    {
        float xRot = Input.GetAxis("TargetHorizontal_Player" + m_playerName);
        float yRot = Input.GetAxis("TargetVertical_Player" + m_playerName);
        float rotation_threshold = 0.1f;
        if (Mathf.Abs(xRot) > rotation_threshold || Mathf.Abs(yRot) > rotation_threshold)
        {
            Vector3 direction = new Vector3(xRot, 0.0f, yRot).normalized;
           
            gameObject.transform.rotation = Quaternion.Slerp(gameObject.transform.rotation, Quaternion.LookRotation(direction, gameObject.transform.up), m_rotationSpeed * Time.deltaTime);
        }
    }

    void Jump()
    {
        if (m_grounded && Input.GetButtonDown("RBumper_Player" + m_playerName))
        {
            m_audioSources[0].Play();
            m_grounded = false;
            m_jumpSpeed = m_jumpStartSpeed;
        }

        if (!m_grounded && m_jumpSpeed > 0.0f)
        {
            m_jumpSpeed -= m_jumpAcceleration * Time.deltaTime;
            gameObject.transform.Translate(new Vector3(0, 1.0f, 0) * m_jumpSpeed * Time.deltaTime);
        }
    }

    void Fall()
    {
        if (!m_grounded)
        {
            if (m_blowHole)
                gameObject.transform.position -= new Vector3(0, 30, 0) * Time.deltaTime;
            else
                gameObject.transform.position -= new Vector3(0, 10, 0) * Time.deltaTime;
        }

        RaycastHit hit;
        int levelMask = (1 << 8) | (1 << 9);
        if (Physics.Raycast(transform.position, -Vector3.up, out hit, levelMask))
        {
            float distanceToGround = hit.distance;
            if (distanceToGround > 1.0f)
            {
                m_grounded = false;
            }
            else
            {
                if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Ground"))
                {
                    if (distanceToGround < 0.5f)
                        gameObject.transform.position += new Vector3(0, distanceToGround, 0);

                    m_grounded = true;
                    m_blowHole = false;
                    m_jumpSpeed = 0.0f;
                }
                else if (hit.collider.gameObject.layer == LayerMask.NameToLayer("BlowHole"))
                {
                    m_jumpSpeed = 120.0f;
                    m_blowHole = true;
                    m_grounded = false;

                    if (m_cubeHolding.m_cube)
                    {
                        GameObject go = Instantiate(Resources.Load("ExplosionSystem") as GameObject) as GameObject;
                        go.renderer.material = renderer.material;
                    }
                    else
                    {
                        GameObject go = Instantiate(Resources.Load("BlowHole") as GameObject) as GameObject;
                    }

                  
                    CountPoints();
                }
            }
        }
        else
            m_grounded = false;
    }

    public void CountPoints()
    {
        CubeHolding holder = GetComponent<CubeHolding>();
        if (holder)
        {
            if( holder.m_cube )
            {
                m_score += 1;
                CubePhysics cphy = holder.m_cube.GetComponent<CubePhysics>();
                if( cphy )
                {
                    cphy.Respawn();
                }
                holder.m_cube = null;
                
            }
        }    
    }


    public void SetAnimation( int state )
    {
        Animator anim = GetComponent<Animator>();
        if (anim)
        {
            anim.SetInteger("StateIndex", state);
        }
    }

    void UpdateAnimations()
    {
        float x = 0.0f, z = 0.0f;
        if (m_allowControl)
        {
            x = Input.GetAxis("MoveHorizontal_Player" + m_playerName);
            z = Input.GetAxis("MoveVertical_Player" + m_playerName);
        }

        // animations...
        if ((Mathf.Abs(x) > threshold || Mathf.Abs(z) > threshold) && m_grounded)
        {
            CubeHolding ch = GetComponent<CubeHolding>();
            if (ch)
            {
                if (ch.m_cube)
                {
                    SetAnimation(2);
                }
                else
                {
                    SetAnimation(1);
                }
            }
            else
            {
                SetAnimation(1);
            }
        }
        else
        {
            CubeHolding ch = GetComponent<CubeHolding>();
            if (ch)
            {
                if (ch.m_cube)
                {
                    SetAnimation(3);
                }
                else
                {
                    SetAnimation(0);
                }
            }
            else
            {
                SetAnimation(0);
            }
        }
    }
}
