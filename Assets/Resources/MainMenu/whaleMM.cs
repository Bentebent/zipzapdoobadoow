﻿using UnityEngine;
using System.Collections;

public class whaleMM : MonoBehaviour 
{
    Vector3[] targets = new Vector3[6];
    int currentTarget = 0;

	// Use this for initialization
	void Start () 
    {
        targets[0] = new Vector3(-1.1f, 62.0f, 13.0f);
        targets[1] = new Vector3(8.5f, 60.0f, 9.0f);
        targets[2] = new Vector3(19.0f, 60.0f, 14.7f);
        targets[3] = new Vector3(15.7f, 60.0f, 25.0f);
        targets[4] = new Vector3(-21.0f, 60.0f, 50.0f);
        targets[5] = new Vector3(-37.0f, 60.0f, 24.6f);
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if (Vector3.Distance(targets[currentTarget], transform.position) < 1.0f)
        {
            currentTarget++;
            currentTarget %= 6;
        }


        Vector3 dir = transform.position - targets[currentTarget];
        Quaternion hurf = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, dir, 10.1f, 0.0f));
        transform.rotation = Quaternion.Slerp(transform.rotation, hurf, Vector3.Distance(transform.position, targets[currentTarget])); 
        transform.position = Vector3.MoveTowards(transform.position, targets[currentTarget], 1.0f * Time.deltaTime);

	}
}
