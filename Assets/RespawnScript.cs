﻿using UnityEngine;
using System.Collections;

public class RespawnScript : MonoBehaviour {

    AudioSource asou;
	// Use this for initialization
    void Start()
    {
        Random.seed = (int)System.DateTime.Now.Ticks;
        asou = GetComponent<AudioSource>();
    }
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        PlayerController pc = other.gameObject.GetComponent<PlayerController>();
        if (pc)
        {
            if (pc.m_lives > 0)
            {
                asou.Play();
                pc.m_lives--;
                pc.gameObject.transform.position = new Vector3(Random.Range(-5.0f, 5.0f), 25.0f, Random.Range(-80.0f, -25.0f));
                pc.m_oldX = 0.0f;
                pc.m_oldZ = 0.0f;
                pc.forces = new Vector3(0.0f, 0.0f, 0.0f);

                PlayerCollision collisionComp = other.GetComponent<PlayerCollision>();
                if (collisionComp)
                {
                    collisionComp.Init();
                }
            }


        }
    }
}
