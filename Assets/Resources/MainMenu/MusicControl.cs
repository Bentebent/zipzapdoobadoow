﻿using UnityEngine;
using System.Collections;

public class MusicControl : MonoBehaviour 
{
    AudioSource mySound;
    public bool fadeIn;
    public bool fadeOut;
	// Use this for initialization
	void Start () 
    {
        mySound = GetComponent<AudioSource>();
        mySound.volume = 0;
        fadeIn = true;
	}
	
	// Update is called once per frame
	void Update () 
    {
	    if(fadeIn)
        {
            mySound.volume += 0.5f * Time.deltaTime;
        }
        else if(fadeOut)
        {
            mySound.volume -= 1.0f * Time.deltaTime;
        }
	}
}
