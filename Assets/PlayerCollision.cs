﻿using UnityEngine;
using System.Collections;

public class PlayerCollision : MonoBehaviour {

    public float m_radius = 3.0f;

    public float m_playerSensitivity = 1.0f;
    public float collisionStrength = 1.0f;

    private AudioSource[] m_audioSources;
	// Use this for initialization
	void Start () {
        Init();
        m_audioSources = GetComponents<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        PlayerController pcont = GetComponent<PlayerController>();
        if (pcont)
        {
            string ownName = pcont.m_playerName;

            GameObject[] cubes;
            cubes = GameObject.FindGameObjectsWithTag("PickableCube");
            foreach (GameObject cube in cubes)
            {
                CubePhysics cphy = cube.GetComponent<CubePhysics>();
                if (cphy)
                {
                    if( cphy.m_state == CubePhysics.CubeState.thown )
                    {
                        if ((gameObject.transform.position - cube.transform.position).magnitude < m_radius)
                        {
                            if (cphy.m_ownerName != ownName)
                            {
                                // player movement
                                Vector3 direction = gameObject.transform.position - cube.transform.position;
                                direction.y = 0.01f;
                                direction = direction.normalized;
                                //direction.y -= 1.0f;
                                //gameObject.transform.position += new Vector3( 0.0f, 1.0f, 0.0f ) * 50.0f * Time.deltaTime;
                                //gameObject.rigidbody.AddForce( direction * 100.0f );

                                //Vector3 direction = gameObject.transform.position - new Vector3( 0.0f, 2.0f, 0.0f );
                                //direction + 

                                gameObject.transform.position += new Vector3( 0.0f, 1.0f, 0.0f ) * 70.0f * Time.deltaTime;
                                pcont.m_jumpSpeed = 70.0f;
                                pcont.m_blowHole = true;
                                pcont.m_grounded = false;
                                pcont.forces = direction * collisionStrength * m_playerSensitivity;


                                // partiklar...
                                GameObject go = Instantiate(Resources.Load("CollisionPuffPlayer" + cphy.m_ownerName) as GameObject) as GameObject;
                                go.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
                                Vector3.RotateTowards(go.transform.position, cube.transform.position, 100.0f, 0.0f);

                                // box movement and reset
                                Vector3 collisionVector = (gameObject.transform.position - cube.transform.position).normalized;
                                cphy.m_speed = -collisionVector * cphy.m_speed.magnitude * 0.08f;
                                cphy.m_state = CubePhysics.CubeState.falling;
                                cphy.m_ownerName = "NoOwner";

                                m_audioSources[1].Play();
                               
                               
                                m_playerSensitivity *= 2.5f;

                                // other cube...
                                CubeHolding ch = GetComponent<CubeHolding>();
                                if( ch )
                                {
                                    if(ch.m_cube)
                                    {
                                        CubePhysics cp = ch.m_cube.GetComponent<CubePhysics>();
                                        if( cp )
                                        {
                                            cp.m_speed = new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(0.3f, 1.0f), Random.Range(-1.0f, 1.0f)).normalized * Random.Range( 2.0f, 6.0f ) * 1.8f;
                                            cp.m_state = CubePhysics.CubeState.falling;
                                            cp.m_ownerName = "NoOwner";
                                            
                                        }
                                        ch.m_cube = null;
                                    }
                                }


                            }
                        }
                    }
                }
            }
        }	
	}

    public void Init()
    {
        m_playerSensitivity = 1.0f;
    }
}
