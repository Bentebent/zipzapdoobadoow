﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainScript : MonoBehaviour 
{

    GUIContent content = new GUIContent();
    public Texture2D image;

    bool mainMenu = true;
    bool setupMenu = false;
    bool gameplay = false;
    bool winMenu = false;

    int nrOfPlayers = 0;
    int lives = 6;
    int scoreLimit = 6;
    int winner = -1;

    bool playerOne = false;
    bool playerTwo = false;
    bool playerThree = false;
    bool playerFour = false;

    GameObject pOneJoinText;
    GameObject pTwoJoinText;
    GameObject pThreeJoinText;
    GameObject pFourJoinText;
    GameObject mainMenuCamera;

    GameObject startMM;
    GameObject exitMM;

    GameObject livesSM;
    GameObject livesNumberSM;
    GameObject startSM;
    GameObject scoreSM;
    GameObject scoreNumberSM;
    GameObject returnSM;


    GameObject bhp;
    GameObject mainLight;
    GameObject respawnPlane;
    GameObject mainCam;
    GameObject whale;
    List<GameObject> players;
    List<PlayerController> playerScripts;


    GameObject pOneScore;
    GameObject pOneLives;
    GameObject pOneScoreNumber;
    GameObject pOneLivesNumber;


    GameObject pTwoScore;
    GameObject pTwoLives;
    GameObject pTwoScoreNumber;
    GameObject pTwoLivesNumber;

    GameObject pThreeScore;
    GameObject pThreeLives;
    GameObject pThreeScoreNumber;
    GameObject pThreeLivesNumber;

    GameObject pFourScore;
    GameObject pFourLives;
    GameObject pFourScoreNumber;
    GameObject pFourLivesNumber;

    GameObject numberOne;
    GameObject numberTwo;
    GameObject numberThree;
    GameObject numberFour;
    GameObject numberFive;
    GameObject numberSix;
    GameObject numberSeven;
    GameObject numberEight;
    GameObject numberNine;
    GameObject numberZero;

    List<GameObject> Clouds;


    GameObject returnWM;
    GameObject winnerWM;

    GameObject playerWM;
    GameObject playerNumberWM;

    List<GameObject> numbers;

    GameObject rainbow;
    GameObject cloudOne;
    GameObject cloudTwo;
    GameObject whaleMM;

    GameObject amerifat;
    GameObject putan;
    GameObject britbong;
    GameObject frog;

	// Use this for initialization
	void Start () 
    {
        Random.seed = (int)System.DateTime.Now.Ticks;
        players = new List<GameObject>();
        playerScripts = new List<PlayerController>();
        numbers = new List<GameObject>();

        exitMM = Instantiate(Resources.Load("MainMenu/ExitGameMM") as GameObject) as GameObject;
        startMM = Instantiate(Resources.Load("MainMenu/StartGameMM") as GameObject) as GameObject;
        mainMenuCamera = Instantiate(Resources.Load("MainMenu/MainMenuCamera") as GameObject) as GameObject;


        numberOne = Instantiate(Resources.Load("Text/NumberOne") as GameObject) as GameObject;
        numberTwo = Instantiate(Resources.Load("Text/NumberTwo") as GameObject) as GameObject;
        numberThree = Instantiate(Resources.Load("Text/NumberThree") as GameObject) as GameObject;
        numberFour = Instantiate(Resources.Load("Text/NumberFour") as GameObject) as GameObject;
        numberFive = Instantiate(Resources.Load("Text/NumberFive") as GameObject) as GameObject;
        numberSix = Instantiate(Resources.Load("Text/NumberSix") as GameObject) as GameObject;
        numberSeven = Instantiate(Resources.Load("Text/NumberSeven") as GameObject) as GameObject;
        numberEight = Instantiate(Resources.Load("Text/NumberEight") as GameObject) as GameObject;
        numberNine = Instantiate(Resources.Load("Text/NumberNine") as GameObject) as GameObject;
        numberZero = Instantiate(Resources.Load("Text/NumberZero") as GameObject) as GameObject;
        
        numbers.Add(numberZero);
        numbers.Add(numberOne);
        numbers.Add(numberTwo);
        numbers.Add(numberThree);
        numbers.Add(numberFour);
        numbers.Add(numberFive);
        numbers.Add(numberSix);
        numbers.Add(numberSeven);
        numbers.Add(numberEight);
        numbers.Add(numberNine);

        mainLight = Instantiate(Resources.Load("MainLight") as GameObject) as GameObject;

        rainbow = Instantiate(Resources.Load("Rainbow") as GameObject) as GameObject;
        cloudOne = Instantiate(Resources.Load("MainMenu/cloudMM1") as GameObject) as GameObject;
        cloudTwo = Instantiate(Resources.Load("MainMenu/cloudMM2") as GameObject) as GameObject;
        whaleMM = Instantiate(Resources.Load("MainMenu/whaleDERP") as GameObject) as GameObject;
	}
	
	// Update is called once per frame
	void Update () 
    {
        for(int i = 0; i < players.Count; i++)
        {
            if (playerScripts[i].m_playerName == "One")
            {
                SetNumber(pOneLivesNumber, playerScripts[i].m_lives);
                SetNumber(pOneScoreNumber, playerScripts[i].m_score);
            }
            else if (playerScripts[i].m_playerName == "Two")
            {
                SetNumber(pTwoLivesNumber, playerScripts[i].m_lives);
                SetNumber(pTwoScoreNumber, playerScripts[i].m_score);
            }
            else if (playerScripts[i].m_playerName == "Three")
            {
                SetNumber(pThreeLivesNumber, playerScripts[i].m_lives);
                SetNumber(pThreeScoreNumber, playerScripts[i].m_score);
            }
            else if(playerScripts[i].m_playerName == "Four")
            {
                SetNumber(pFourLivesNumber, playerScripts[i].m_lives);
                SetNumber(pFourScoreNumber, playerScripts[i].m_score);
            }
            if(playerScripts[i].m_score >= scoreLimit && gameplay)
            {
                winner = i;
                winMenu = true;
                gameplay = false;

                returnWM = Instantiate(Resources.Load("EndMenu/ReturnWM") as GameObject) as GameObject;
                winnerWM = Instantiate(Resources.Load("EndMenu/WinnerWM") as GameObject) as GameObject;

                playerWM = Instantiate(Resources.Load("EndMenu/PlayerWM") as GameObject) as GameObject;
                playerNumberWM = Instantiate(Resources.Load("EndMenu/PlayerNumberWM") as GameObject) as GameObject;

                winnerWM.renderer.material = players[i].renderer.material;
                playerWM.renderer.material = players[i].renderer.material;
                playerNumberWM.renderer.material = players[i].renderer.material;

                if (playerScripts[i].m_playerName == "One")
                    SetNumber(playerNumberWM, 1);
                else if (playerScripts[i].m_playerName == "Two")
                    SetNumber(playerNumberWM, 2);
                else if (playerScripts[i].m_playerName == "Three")
                    SetNumber(playerNumberWM, 3);
                else if (playerScripts[i].m_playerName == "Four")
                    SetNumber(playerNumberWM, 4);
            }
        }

        if (nrOfPlayers <= 1)
        {
            gameplay = false;
        }


        if (setupMenu)
        {
         
            if (Input.GetButtonDown("Start_PlayerOne") && !playerOne)
            {
                playerOne = true;
                nrOfPlayers++;
                DestroyImmediate(pOneJoinText);
                britbong = Instantiate(Resources.Load("MainMenu/BritBongPortrait") as GameObject) as GameObject;
            }

            if (Input.GetButtonDown("Start_PlayerTwo") && !playerTwo)
            {
                playerTwo = true;
                nrOfPlayers++;
                DestroyImmediate(pTwoJoinText);
                frog = Instantiate(Resources.Load("MainMenu/FrogPortrait") as GameObject) as GameObject;
            }

            if (Input.GetButtonDown("Start_PlayerThree") && !playerThree)
            {
                playerThree = true;
                nrOfPlayers++;
                DestroyImmediate(pThreeJoinText);
                amerifat = Instantiate(Resources.Load("MainMenu/AmerifatPortrait") as GameObject) as GameObject;
            }

            if (Input.GetButtonDown("Start_PlayerFour") && !playerFour)
            {
                playerFour = true;
                nrOfPlayers++;
                DestroyImmediate(pFourJoinText);
                putan = Instantiate(Resources.Load("MainMenu/PutanPortrait") as GameObject) as GameObject;
            }
        }
	}

    void OnGUI()
    {
        if (gameplay)
            return;

        if (mainMenu)
        {
            MainMenu();
        }
        else if (setupMenu)
        {
            SetupMenu();
        }
        else if (winMenu)
        {
            WinMenu();
        }

    }

    private void WinMenu()
    {
        GUI.Label(new Rect(Screen.width / 2.0f - 100.0f, Screen.height / 2.0f - 75.0f / 2.0f - 200.0f, 200, 75.0f), "", GUIStyle.none);
        if (GUI.Button(new Rect(Screen.width / 2.0f - 190.0f, Screen.height / 2.0f - 90.0f / 2.0f + 200.0f, 400, 90.0f), "", GUIStyle.none))
        {
            mainMenu = true;
            setupMenu = false;
            winMenu = false;

            DestroyImmediate(bhp);
            DestroyImmediate(respawnPlane);
            DestroyImmediate(whale);
            foreach( GameObject cloud in Clouds )
            {
                DestroyImmediate(cloud);
            }

            for (int i = 0; i < nrOfPlayers; i++)
            {
                DestroyImmediate(players[i]);
                DestroyImmediate(playerScripts[i]);
            }

            players.Clear();
            playerScripts.Clear();

            GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("PickableCube");

            for (var i = 0; i < gameObjects.Length; i++)
                Destroy(gameObjects[i]);

            nrOfPlayers = 0;

            exitMM = Instantiate(Resources.Load("MainMenu/ExitGameMM") as GameObject) as GameObject;
            startMM = Instantiate(Resources.Load("MainMenu/StartGameMM") as GameObject) as GameObject;
            mainMenuCamera = Instantiate(Resources.Load("MainMenu/MainMenuCamera") as GameObject) as GameObject;

            MusicControl mc = mainCam.GetComponent<MusicControl>();
            mc.fadeOut = true;
            mc.fadeIn = false;
            mainCam.transform.position = mainMenuCamera.transform.position;
            mainCam.transform.rotation = mainMenuCamera.transform.rotation;
            mainCam.camera.farClipPlane = 100.0f;
            Destroy(mainCam, 1.0f);

            DestroyImmediate(pOneScore);
            DestroyImmediate(pOneLives);
            DestroyImmediate(pTwoScore);
            DestroyImmediate(pTwoLives);
            DestroyImmediate(pThreeScore);
            DestroyImmediate(pThreeLives);
            DestroyImmediate(pFourScore);
            DestroyImmediate(pFourLives);

            DestroyImmediate(pOneScoreNumber);
            DestroyImmediate(pOneLivesNumber);
            DestroyImmediate(pTwoScoreNumber);
            DestroyImmediate(pTwoLivesNumber);
            DestroyImmediate(pThreeScoreNumber);
            DestroyImmediate(pThreeLivesNumber);
            DestroyImmediate(pFourScoreNumber);
            DestroyImmediate(pFourLivesNumber);

            DestroyImmediate(returnWM);
            DestroyImmediate(winnerWM);
            DestroyImmediate(playerWM);
            DestroyImmediate(playerNumberWM);

            rainbow = Instantiate(Resources.Load("Rainbow") as GameObject) as GameObject;
            cloudOne = Instantiate(Resources.Load("MainMenu/cloudMM1") as GameObject) as GameObject;
            cloudTwo = Instantiate(Resources.Load("MainMenu/cloudMM2") as GameObject) as GameObject;
            whaleMM = Instantiate(Resources.Load("MainMenu/whaleDERP") as GameObject) as GameObject;
        }
    }

    private void MainMenu()
    {
        if (GUI.Button(new Rect(Screen.width / 2.0f - 400.0f, Screen.height / 2.0f - 100.0f, 800, 120.0f), "", GUIStyle.none))
        {
            mainMenu = false;
            setupMenu = true;

            pOneJoinText = Instantiate(Resources.Load("MainMenu/POneJoinText") as GameObject) as GameObject;
            pTwoJoinText = Instantiate(Resources.Load("MainMenu/PTwoJoinText") as GameObject) as GameObject;
            pThreeJoinText = Instantiate(Resources.Load("MainMenu/PThreeJoinText") as GameObject) as GameObject;
            pFourJoinText = Instantiate(Resources.Load("MainMenu/PFourJoinText") as GameObject) as GameObject;

            livesSM = Instantiate(Resources.Load("MainMenu/LivesSM") as GameObject) as GameObject;
            startSM = Instantiate(Resources.Load("MainMenu/StartGameSM") as GameObject) as GameObject;
            scoreSM = Instantiate(Resources.Load("MainMenu/ScoreLimitSM") as GameObject) as GameObject;
            returnSM = Instantiate(Resources.Load("MainMenu/ReturnSM") as GameObject) as GameObject;

            livesNumberSM = Instantiate(Resources.Load("MainMenu/LivesNumberSM") as GameObject) as GameObject;
            scoreNumberSM = Instantiate(Resources.Load("MainMenu/ScoreNumberSM") as GameObject) as GameObject;
          
            DestroyImmediate(exitMM);
            DestroyImmediate(startMM);
        }

        if (GUI.Button(new Rect(Screen.width / 2.0f - 150.0f, Screen.height / 2.0f - 75.0f / 2.0f + 180.0f, 300, 100.0f), "", GUIStyle.none))
        {
            Application.Quit();
        }
    }

    private void SetupMenu()
    {
        if (GUI.Button(new Rect(Screen.width / 2.0f - 300.0f, Screen.height / 2.0f - 100.0f / 2.0f - 300.0f, 600, 100.0f), "", GUIStyle.none) && nrOfPlayers >= 2)
        {
            DestroyImmediate(pOneJoinText);
            DestroyImmediate(pTwoJoinText);
            DestroyImmediate(pThreeJoinText);
            DestroyImmediate(pFourJoinText);

            DestroyImmediate(livesSM); 
            DestroyImmediate(startSM);
            DestroyImmediate(scoreSM);
            DestroyImmediate(returnSM);
            DestroyImmediate( livesNumberSM);
            DestroyImmediate(scoreNumberSM);


            for (int i = 0; i < nrOfPlayers; i++)
            {
                GameObject go = null;// = Instantiate(Resources.Load("Player") as GameObject) as GameObject;
                PlayerController po = null;// = go.GetComponent<PlayerController>();
               
                if (playerOne)
                {
                    go = Instantiate(Resources.Load("PlayerBritBong") as GameObject) as GameObject;
                    po = go.GetComponent<PlayerController>();
                    po.m_playerName = "One";
                    playerOne = false;

                    pOneScore = Instantiate(Resources.Load("GameplayGUI/pOneScore") as GameObject) as GameObject;
                    pOneLives = Instantiate(Resources.Load("GameplayGUI/pOneLives") as GameObject) as GameObject;

                    pOneScoreNumber = Instantiate(Resources.Load("GameplayGUI/pOneScoreNumber") as GameObject) as GameObject;
                    pOneLivesNumber = Instantiate(Resources.Load("GameplayGUI/pOneLivesNumber") as GameObject) as GameObject;
                }
                else if (playerTwo)
                {
                    go = Instantiate(Resources.Load("PlayerFrog") as GameObject) as GameObject;
                    po = go.GetComponent<PlayerController>();
                    po.m_playerName = "Two";
                    playerTwo = false;
                    pTwoScore = Instantiate(Resources.Load("GameplayGUI/pTwoScore") as GameObject) as GameObject;
                    pTwoLives = Instantiate(Resources.Load("GameplayGUI/pTwoLives") as GameObject) as GameObject;

                    pTwoScoreNumber = Instantiate(Resources.Load("GameplayGUI/pTwoScoreNumber") as GameObject) as GameObject;
                    pTwoLivesNumber = Instantiate(Resources.Load("GameplayGUI/pTwoLivesNumber") as GameObject) as GameObject;
                }
                else if (playerThree)
                {
                    go = Instantiate(Resources.Load("PlayerAmerifat") as GameObject) as GameObject;
                    po = go.GetComponent<PlayerController>();
                    po.m_playerName = "Three";
                    playerThree = false;
                    pThreeScore = Instantiate(Resources.Load("GameplayGUI/pThreeScore") as GameObject) as GameObject;
                    pThreeLives = Instantiate(Resources.Load("GameplayGUI/pThreeLives") as GameObject) as GameObject;

                    pThreeScoreNumber = Instantiate(Resources.Load("GameplayGUI/pThreeScoreNumber") as GameObject) as GameObject;
                    pThreeLivesNumber = Instantiate(Resources.Load("GameplayGUI/pThreeLivesNumber") as GameObject) as GameObject;
                }
                else if (playerFour)
                {
                    go = Instantiate(Resources.Load("PlayerPutin") as GameObject) as GameObject;
                    po = go.GetComponent<PlayerController>();
                    po.m_playerName = "Four";
                    playerFour = false;
                    pFourScore = Instantiate(Resources.Load("GameplayGUI/pFourScore") as GameObject) as GameObject;
                    pFourLives = Instantiate(Resources.Load("GameplayGUI/pFourLives") as GameObject) as GameObject;

                    pFourScoreNumber = Instantiate(Resources.Load("GameplayGUI/pFourScoreNumber") as GameObject) as GameObject;
                    pFourLivesNumber = Instantiate(Resources.Load("GameplayGUI/pFourLivesNumber") as GameObject) as GameObject;
                }

                po.m_lives = lives;
                go.gameObject.transform.position = new Vector3(Random.Range(-5.0f, 5.0f), 25.0f, Random.Range(-80.0f, -25.0f));


                players.Add(go);
                playerScripts.Add(po);

                DestroyImmediate(rainbow);
                DestroyImmediate(cloudOne);
                DestroyImmediate(cloudTwo);
                DestroyImmediate(whaleMM);

                DestroyImmediate(amerifat);
                DestroyImmediate(putan);
                DestroyImmediate(britbong);
                DestroyImmediate(frog);

            }

            bhp = Instantiate(Resources.Load("BlowHolePlane") as GameObject) as GameObject;
           
            respawnPlane = Instantiate(Resources.Load("RespawnPlane") as GameObject) as GameObject;
            whale = Instantiate(Resources.Load("whale") as GameObject) as GameObject;
            mainCam = Instantiate(Resources.Load("Main Camera") as GameObject) as GameObject;

            int nrClouds = 1000;
            Clouds = new List<GameObject>();
            for (int i = 0; i < nrClouds; i++)
            {
                Clouds.Add(Instantiate(Resources.Load("cloud") as GameObject) as GameObject);
            }
            //Cloud = Instantiate(Resources.Load("cloud") as GameObject) as GameObject;


            MusicControl mc = mainMenuCamera.GetComponent<MusicControl>();
            mc.fadeOut = true;
            mc.fadeIn = false;
            mainMenuCamera.transform.position = mainCam.transform.position;
            mainMenuCamera.transform.rotation = mainCam.transform.rotation;
            mainMenuCamera.camera.farClipPlane = 1000.0f;
            Destroy(mainMenuCamera, 1.0f);

            gameplay = true;
            mainMenu = false;
            setupMenu = false;

            

        }

        if (GUI.Button(new Rect(Screen.width / 2.0f - 150.0f, Screen.height / 2.0f - 90.0f / 2.0f - 110, 300, 90.0f), "", GUIStyle.none))
        {
            scoreLimit++;
            if (scoreLimit == 10)
                scoreLimit = 1;
            SetNumber(scoreNumberSM, scoreLimit);
        }

        
        if (GUI.Button(new Rect(Screen.width / 2.0f - 125.0f, Screen.height / 2.0f - 100.0f / 2.0f + 25, 250, 100.0f), "", GUIStyle.none))
        {
            lives++;
            if (lives == 10)
                lives = 1;
            SetNumber(livesNumberSM, lives);
        }


        if (GUI.Button(new Rect(Screen.width / 2.0f - 180.0f, Screen.height / 2.0f - 90.0f / 2.0f + 170.0f, 360, 90.0f), "", GUIStyle.none))
        {
            mainMenu = true;
            setupMenu = false;

            DestroyImmediate(pOneJoinText);
            DestroyImmediate(pTwoJoinText);
            DestroyImmediate(pThreeJoinText);
            DestroyImmediate(pFourJoinText);

            DestroyImmediate(livesSM);
            DestroyImmediate(startSM);
            DestroyImmediate(scoreSM);
            DestroyImmediate(returnSM);

            DestroyImmediate(amerifat);
            DestroyImmediate(putan);
            DestroyImmediate(britbong);
            DestroyImmediate(frog);

            exitMM = Instantiate(Resources.Load("MainMenu/ExitGameMM") as GameObject) as GameObject;
            startMM = Instantiate(Resources.Load("MainMenu/StartGameMM") as GameObject) as GameObject;
        }

    }

    private void SetNumber(GameObject go, int number)
    {
        if (number > 9)
            return;
        MeshFilter mf = numbers[number].GetComponent<MeshFilter>();
        go.GetComponent<MeshFilter>().mesh = mf.mesh;
    }
}
